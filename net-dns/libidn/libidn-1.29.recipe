SUMMARY="Internationalized Domain Names (IDN) implementation."
DESCRIPTION="
Libidn is a package for internationalized string handling based on the \
Stringprep, Punycode, IDNA and TLD specifications.
"
HOMEPAGE="http://www.gnu.org/software/libidn"
COPYRIGHT="
	2002-2014 Simon Josefsson
	"
LICENSE="GNU LGPL v2.1"
SRC_URI="http://ftp.gnu.org/gnu/libidn/libidn-$portVersion.tar.gz"
CHECKSUM_SHA256="fb82747dbbf9b36f703ed27293317d818d7e851d4f5773dedf3efa4db32a7c7c"
REVISION="1"
ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	libidn$secondaryArchSuffix = $portVersion compat >= 1
	lib:libidn$secondaryArchSuffix = 11.6.12 compat >= 11
	cmd:idn$secondaryArchSuffix
	"
REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libiconv$secondaryArchSuffix
	lib:libintl$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libiconv$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:libtoolize
	cmd:make
	cmd:gettext >= 0.18.3
	cmd:find
	"

BUILD()
{
	autoreconf -fi
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libidn
	fixPkgconfig

	# devel package
	packageEntries devel \
		$developDir \
		$manDir/man3
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	libidn${secondaryArchSuffix}_devel = $portVersion compat >= 1
	devel:libidn$secondaryArchSuffix = 11.6.12 compat >= 11
	"
REQUIRES_devel="
	libidn$secondaryArchSuffix == $portVersion base
	"
