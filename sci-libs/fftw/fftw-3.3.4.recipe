SUMMARY="Fast C library for the Discrete Fourier Transform"
DESCRIPTION="
FFTW is a C subroutine library for computing the discrete Fourier transform \
(DFT) in one or more dimensions, of arbitrary input size, and of both real and \
complex data (as well as of even/odd data, i.e. the discrete cosine/sine \
transforms or DCT/DST). We believe that FFTW, which is free software, should \
become the FFT library of choice for most applications.
Our benchmarks, performed on on a variety of platforms, show that FFTW's \
performance is typically superior to that of other publicly available FFT \
software, and is even competitive with vendor-tuned codes. In contrast to \
vendor-tuned codes, however, FFTW's performance is portable: the same program \
will perform well on most architectures without modification. Hence the name, \
FFTW, which stands for the somewhat whimsical title of Fastest Fourier \
Transform in the West.
"
HOMEPAGE="http://www.fftw.org/"
SRC_URI="http://www.fftw.org/fftw-$portVersion.tar.gz"
CHECKSUM_SHA256="8f0cde90929bc05587c3368d2f15cd0530a60b8a9912a8e2979a72dbe5af0982"
LICENSE="GNU GPL v2"
COPYRIGHT="
	2003, 2007-2011 Matteo Frigo
	2003, 2007-2011 Massachusetts Institute of Technology
	"
REVISION="3"
ARCHITECTURES="x86 x86_gcc2 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	libfftw$secondaryArchSuffix = $portVersion compat >= 3
	lib:libfftw$secondaryArchSuffix = $portVersion compat >= 3
	cmd:fftw_wisdom$secondaryArchSuffix
	cmd:fftw_wisdom_to_conf$secondaryArchSuffix
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

BUILD_REQUIRES="
	"

BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:awk
	cmd:find
	cmd:make
	"

BUILD()
{
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install
	prepareInstalledDevelLib libfftw3
	fixPkgconfig
	packageEntries devel $developDir
}

PROVIDES_devel="
	libfftw${secondaryArchSuffix}_devel = $portVersion compat >= 3
	devel:libfftw$secondaryArchSuffix = $portVersion compat >= 3
	devel:libfftw3$secondaryArchSuffix = $portVersion compat >= 3
	"
REQUIRES_devel="
	libfftw$secondaryArchSuffix == $portVersion base
	"
