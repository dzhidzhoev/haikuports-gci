SUMMARY="More than a near-exact clone of the classic Windows game."
DESCRIPTION="
It's about time we had more than a near-exact clone of the classic Windows game,
Minesweeper.

Controls
- Left click: Reveal a box. Clicking on a mine will lose the game.
- Right click: Change a box to either unmarked, flagged as a mine, or a question mark.
- Middle click: Sonar ping which reveals all mines in a 3x3 square without the \
	danger of setting them off. It comes with a 20 second cost added to your \
	time. Left clicking while holding down the Alt key will also will do this.
- Click with both buttons: If the tile is a number and the appropriate number \
	of mines around it have been flagged, it will clear all the other squares \
	around the number. Of course, if you've made a mistake, you'll probably \
	set off a mine. Left clicking while holding down the Shift key will also \
	do this."
HOMEPAGE="https://github.com/HaikuArchives/BeMines"
SRC_URI="git+https://github.com/haikuarchives/bemines#be29173f854c82f5b312ed31b5245b807a5afe0f"
LICENSE="MIT"
COPYRIGHT="2009 Jon Yoder"
ARCHITECTURES="x86_gcc2"
REVISION="1"

PATCHES="bemines-$portVersion.patchset"

PROVIDES="
	bemines = $portVersion
	app:bemines
"

BUILD_PREREQUIRES="
	haiku_devel >= $haikuVersion
	cmd:gcc
"

BUILD() {
	gcc src/*.cpp -o BeMines -lbe -ltranslation -lmedia
	xres -o BeMines src/BeMines.rsrc
}

INSTALL() {
	mkdir -p $appsDir/BeMines
	cp BeMines $appsDir/BeMines
	cp -r src/themes $appsDir/BeMines
	addAppDeskbarSymlink $appsDir/BeMines/BeMines BeMines
}
