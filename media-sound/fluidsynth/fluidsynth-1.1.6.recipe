SUMMARY="Fluidsynth is a software real-time synthesizer"
DESCRIPTION="
Fluidsynth is a software real-time synthesizer based on the Soundfont 2 \
specifications.
"
HOMEPAGE="http://www.fluidsynth.org/" 
SRC_URI="http://sourceforge.net/projects/fluidsynth/files/fluidsynth-$portVersion/fluidsynth-$portVersion.tar.bz2"
CHECKSUM_SHA256="d28b47dfbf7f8e426902ae7fa2981d821fbf84f41da9e1b85be933d2d748f601"
LICENSE="GNU LGPL v2.1"
COPYRIGHT="2007-2012 Josh Green, Pedro Lopez-Cabanillas, David Henningsson"

REVISION="4"
ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PATCHES="fluidsynth-1.1.6.patchset"

PROVIDES="
	fluidsynth${secondaryArchSuffix} = $portVersion
	lib:libfluidsynth${secondaryArchSuffix} = 1.5.2 compat >= 1
	cmd:fluidsynth${secondaryArchSuffix}
	"

REQUIRES="
	haiku${secondaryArchSuffix} >= $haikuVersion
	lib:libFLAC$secondaryArchSuffix
	lib:libglib_2.0$secondaryArchSuffix
	lib:libiconv$secondaryArchSuffix
	lib:libintl$secondaryArchSuffix
	lib:libncurses$secondaryArchSuffix
	lib:libogg$secondaryArchSuffix
	lib:libreadline$secondaryArchSuffix
	lib:libsndfile$secondaryArchSuffix
	lib:libvorbis$secondaryArchSuffix
	"

BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libFLAC$secondaryArchSuffix
	devel:libglib_2.0$secondaryArchSuffix
	devel:libiconv$secondaryArchSuffix
	devel:libncurses$secondaryArchSuffix
	devel:libogg$secondaryArchSuffix
	devel:libreadline$secondaryArchSuffix
	devel:libsndfile$secondaryArchSuffix
	devel:libvorbis$secondaryArchSuffix
	"

BUILD_PREREQUIRES="
	cmd:gcc$secondaryArchSuffix
	cmd:libtoolize
	cmd:pkg_config$secondaryArchSuffix
	cmd:make
	cmd:awk
	cmd:aclocal
	cmd:autoconf
	cmd:find
	"

BUILD()
{
	libtoolize -fci

	./autogen.sh

	runConfigure ./configure
	
	make $jobArgs
}

INSTALL()
{
	make install
	
	prepareInstalledDevelLib libfluidsynth
	fixPkgconfig
	
	# devel package
	packageEntries devel \
		$developDir

}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	fluidsynth${secondaryArchSuffix}_devel = $portVersion
	devel:libfluidsynth$secondaryArchSuffix = 1.5.2 compat >= 1
	"
REQUIRES_devel="
	fluidsynth$secondaryArchSuffix == $portVersion base
	"
