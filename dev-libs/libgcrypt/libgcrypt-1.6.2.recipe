SUMMARY="GNU's basic cryptographic library."
DESCRIPTION="
Libgcrypt is a general purpose crypto library based on the code \
used in GnuPG.
"
LICENSE="GNU LGPL v3"
COPYRIGHT="2000-2013 Free Software Foundation, Inc."
HOMEPAGE="http://directory.fsf.org/project/libgcrypt/" 
SRC_URI="ftp://ftp.gnupg.org/gcrypt/libgcrypt/libgcrypt-$portVersion.tar.bz2"
CHECKSUM_SHA256="de084492a6b38cdb27b67eaf749ceba76bf7029f63a9c0c3c1b05c88c9885c4c"
REVISION="1"
ARCHITECTURES="x86_gcc2 x86 x86_64"

PATCHES="libgcrypt-$portVersion.patchset"

PROVIDES="
	libgcrypt${secondaryArchSuffix} = $portVersion compat >= 1.6
	lib:libgcrypt${secondaryArchSuffix} = 20.0.2 compat >= 20
	"

REQUIRES="
	haiku${secondaryArchSuffix} >= $haikuVersion
	lib:libgpg_error${secondaryArchSuffix}
	"

BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libgpg_error${secondaryArchSuffix}
	"

BUILD_PREREQUIRES="
	cmd:autoconf
	cmd:make
	cmd:gcc${secondaryArchSuffix}
	"

BUILD()
{
	autoconf
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libgcrypt

	packageEntries devel \
		$developDir $binDir
}

PROVIDES_devel="
	libgcrypt${secondaryArchSuffix}_devel = $portVersion compat >= 1.6
	devel:libgcrypt$secondaryArchSuffix = 20.0.2 compat >= 20
	cmd:dumpsexp$secondaryArchSuffix = $portVersion compat >= 1.6
	cmd:libgcrypt_config$secondaryArchSuffix = $portVersion compat >= 1.6
	cmd:hmac256$secondaryArchSuffix = $portVersion compat >= 1.6
	cmd:mpicalc$secondaryArchSuffix = $portVersion compat >= 1.6
	"

REQUIRES_devel="
	libgcrypt${secondaryArchSuffix} == $portVersion base
	haiku${secondaryArchSuffix} >= $haikuVersion
	libgpg_error${secondaryArchSuffix}
	"
